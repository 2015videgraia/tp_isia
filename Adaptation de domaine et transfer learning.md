# Adaptation de domaine et transfer learning

L'objectif de l'apprentissage automatique est de fournir à un programme de nombreuses données annotées par des humains pour lui faire apprendre une tâche.

Le problème c'est qu'il existe des domaines avec peu de données annotées, ou qui changent avec le temps.

Pour se défaire de ce problème plusieurs approches sont possible.

Notamment le transfert d'apprentissage. L'idée est de réutiliser des systèmes construits pour des usages différents mais liés.

Les “classifiers” optimisés sur un ensemble de données de référence présentent souvent une dégradation significative lorsqu'ils sont évalués sur un autre ensemble.

Plusieurs méthodes sont disponibles pour remédier à cela.

![40% center](Images/Capture_d_écran_2020-03-02_à_16.45.59.png )

## Situation initiale
On a :
* Des caractéristiques X, des étiquettes Y
* Une distribution Ps sur X*Y, Pt sur X*Y
* Une fonction de labelling f(X) = Y qui suit Pt(y|x)
* Un ensemble d’entrainement source LS
* Un ensemble non labellisé cible LT

A partir de là, on veut apprendre un classificateur hypothèse h aussi proche que possible de f qui soit bon sur la distribution Pt.

## Comment évaluer les performances de l’adaptation ?
L’erreur de classification sur le domaine cible peut provenir de trois sources :
* Erreur du classificateur d’origine sur le domaine source
* Mesure de la divergence entre les deux domaines
* Un terme empirique (qu’on espère faible) qui traduit à quel point les tâches sont liées

## Méthode de reweighting
Une première famille de méthode consiste à apprendre le nouveau modèle en réévaluant les poids des données sources d’après une estimation de l’erreur sur le domaine source.

Afin de garantir la vraisemblance de cette méthode, on fait l’hypothèse que le dataset cible correspond au dataset source ayant subi un covariate shift, hypothèse qui dit que les variables qui changent d’un dataset à l’autre sont des variables indépendantes. 

Cependant cette hypothèse étant forte, le résultat peut ne pas être concluant.

La qualité de l'adaptation dépend de l'importance des poids.

## Ajustement et méthodes itératives
Un autre ensemble de méthodes consiste à labelliser les données cibles d’une certaine manière.
On assigne des pseudo étiquettes aux données de l’ensemble cible de façon itérative, de plus en plus sûr.

![40% center](Images/Capture_d_écran_2020-03-05_à_14.55.26.png)

Le problème de cette méthode est qu’on a aucune garantie de convergence de l’algorithme qui assigne les pseudo étiquettes.

## Alignement de sous-espaces

Cette technique consiste à extraire depuis l’ensemble source et l’ensemble cible des sous espaces de caractéristiques, et de trouver une transformation linéaire qui lie ces deux bases de représentation ensemble.

Cette méthode a l’avantage d’être claire.
L’inconvénient de cette méthode est qu’elle suggère que les instances choisies dans les deux ensembles sont pertinentes pour la tâche étudiée.

## Adaptation de domaine
L’idée de cette méthode est de trouver une une fonction qui transforme un sous-ensemble de points de l’ensemble source en un autre sous-ensemble de l’ensemble cible, de telle sorte que le transport d’un sous ensemble à un autre se fasse sur des distances minimales pour le sous ensemble considéré

![40% center](Images/Capture_d_écran_2020-03-05_à_15.15.52.png)

## Methode deep learning

Considérant un réseau de neurone y=f(x) 


On enlève la dernière couche ( qui correspond a l’approximation de y fournit par le réseau ) et on on considere toutes les couches (i=1:n-1, nommé : pretrained layers ) et en les considère comme une seule couche d'entrée ( pour garder les poids trouvés a l’aide de systeme precedent) et on entraîne notre nouveau réseau ( on trouve le Wij) 
dont le système sera considéré comme :   



## Transfert d’hypothèse
Une des limitations des méthodes proposées jusqu’ici est que pour les appliquer, il faut garder le dataset d’origine pour calculer les transformations d'étiquetage ou de prédiction. 

Une méthode permettant de s’affranchir de cette contrainte est le transfert d’hypothèse. C’est la fonction d’évaluation elle même qui est transféré plutôt que son dataset.

# Conclusion
Domaines très actifs - Beaucoup de méthodes (parfois difficiles à suivre)

Idée globale : Rapprocher les distributions tout en assurant une bonne exactitude des données étiquetées.

L'adaptation de domaine est difficile et il est donc presque impossible d’imaginer des cadres de travail généraux.

On choisit une méthode en fonction de la tâche/données et il est important de bien préparer les données.


### Sources

https://epat2014.sciencesconf.org/conference/

https://limos.fr/media/uploads/seminaire/

http://nlp.cs.rpi.edu/paper/qisurvey.pdf

https://www.aclweb.org/anthology/K17-1029/

https://arxiv.org/pdf/1911.02655.pdf

https://www.aclweb.org/anthology/N18-1143.pdf